import {Breakout} from './Breakout';
import {StateGameplay} from './StateGameplay';


const BRICK_FRAMES : string[] =['brick_1.png','brick_2.png','brick_3.png' ];

export class Brick extends Phaser.Sprite
{
    game  : Breakout.BreakoutGame;
    scene : StateGameplay;

    Duration : number;
    breakout : Breakout.BreakoutGame;

    constructor(game : Breakout.BreakoutGame , scene : StateGameplay , 
                x:number,y:number , duration :number)
    {

        

        super(game,x,y,'atlas','brick_'+duration+'.png');

        this.scene = scene;
        this.game     = game;

        this.anchor.set(0);

        this.x = x;
        this.y = y;

        this.Duration = duration;

        this.animations.add('Types', BRICK_FRAMES  );

        this.animations.play('Types');

        this.game.physics.arcade.enable(this);

        this.body.bounce.set(1);
        this.body.immovable = true;

        

    }
    update()
    {
        this.checkFrame();
    }
    checkFrame()
    {
        this.animations.frameName  = BRICK_FRAMES[this.Duration-1];
    }
    onHit()
    {
        this.Duration --;

        if(this.Duration > 0)
        {
            this.checkFrame();
  
        }
        else
        {
            this.scene.onBrickDestroyed();
            this.destroy();
        }
    }

}