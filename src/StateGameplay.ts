
import {Breakout} from './Breakout';
import {Brick} from './Brick';
import {Player} from './Player';
import {Ball} from './Ball';


const BRICK_WIDTH  : number = 45;
const BRICK_HEIGHT : number = 23;


//-- todo: replace with a proper file of Levels
//-- [0,0,0,0,0,0,0,0,0,0] empty for quicker editing copy/paset
let Level_Test : number[][] = [
    [0,0,2,0,0,0,0,2,0,0],
    [0,0,2,0,0,0,0,2,0,0],
    [0,0,1,1,1,1,1,1,0,0],
    [0,1,1,3,1,1,3,1,1,0],
    [1,1,1,1,1,1,1,1,1,1],
    [1,0,1,1,1,1,1,1,0,1],
    [1,0,1,0,0,0,0,1,0,1],
    [0,0,1,1,0,0,1,1,0,0],
    
];


export enum EGAMESTATE
{
    WaitingForShoot,
    Playing,
    Finished    
}

export class StateGameplay extends Phaser.State{

    game : Breakout.BreakoutGame;

    background : any;

    bricks : Phaser.Group;

    player : Player;
    ball   : Ball;

    shootkey : Phaser.Key;

    totalBricks : number;

    gameState : number;

    lives : number;

    textLives ;
    textCentral ;

    constructor() {
        super();

        var me = this;
    }
    
    preload()
    {
            console.log("[StatePlaying] Loading Assets");

            this.game.load.atlas('atlas','assets/atlas.png','assets/atlas.json');

    }
    create  ()
    {
        
        this.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.bricks = this.game.add.group();
        this.game.physics.arcade.checkCollision.down = false;
        

        //-- Create Player 
        
        this.player = new Player( this.game);
        this.game.add.existing(this.player);
       
        //-- Create Ball
        this.ball = new Ball(this.game , this );
        this.game.add.existing(this.ball);

        this.game.input.onDown.add( this.onInputDown ,this);

        this.textLives = this.game.add.text(10, 0, 'lives: 3', { font: "22px Arial", fill: "#ffffff", align: "left" });
        this.textCentral = this.game.add.text(
                            this.game.world.centerX, 350, 'Tap to Start', 
                            { font: "30px Arial",fill: "#ffffff", align: "center" }
                            );
        
        this.textCentral.anchor.setTo(0.5);

        this.resetGame();

    }
    resetGame()
    {
        this.lives = 3;
        this.gameState = EGAMESTATE.WaitingForShoot;

        //------ Bricks Creation 
        this.bricks.destroy();
        this.bricks = this.game.add.group();

        this.totalBricks = 0;

        //-- todo: replace by a proper Level Selector
        let SelectedLevel : number[][] = Level_Test; 

        let Level_X_Qty : number = SelectedLevel[0].length;
        let Level_Y_Qty : number = SelectedLevel.length;

        this.bricks.x = (this.game.world.centerX)  - (SelectedLevel[0].length * BRICK_WIDTH)/2 ;
        this.bricks.y = 100;
        
        for(var y = 0 ; y < Level_Y_Qty ; y++)
        {
            for(var x = 0 ; x < Level_X_Qty ; x++)
            {
                
                let BrickNumber : number = Level_Test[y][x];
                if(BrickNumber!=0)
                {
                    let brick = new Brick( this.game ,this, (x*BRICK_WIDTH), (y*BRICK_HEIGHT) , BrickNumber);
                    this.bricks.add(brick);
                    this.totalBricks++;    
                }
            }      
        }

        let LevelWidht: number = BRICK_WIDTH * 10;

        this.bricks.enableBody = true;
        this.bricks.physicsBodyType = Phaser.Physics.ARCADE;
        
        this.textCentral.text = "Tap to Start";
        
    }

    onInputDown()
    {

        switch(this.gameState)
        {
            case EGAMESTATE.WaitingForShoot:
                this.textCentral.text = '';

                this.gameState = EGAMESTATE.Playing;
                this.ball.launch();
            break;
            case EGAMESTATE.Playing:
        
            break;
            case EGAMESTATE.Finished:
                this.resetGame();
                this.gameState = EGAMESTATE.WaitingForShoot;
            break;
        }
        
    }

    update()
    {
        switch(this.gameState)
        {
            case EGAMESTATE.WaitingForShoot:
                this.updateState_WaitingForShoot();
            break;
            case EGAMESTATE.Playing:
                this.updateState_Playing();
            break;
            case EGAMESTATE.Finished:
                this.updateState_GameOver();
            break;
        }
        


    }
    updateState_WaitingForShoot ()
    {
       
    }
    updateState_Playing ()
    {
        this.game.physics.arcade.collide(
            this.ball,this.player,
            ()=>{ this.ball.onHitPlayer(); },
            null,this);
    }
    updateState_GameOver ()
    {
        
    }
    onBrickDestroyed()
    {
        this.totalBricks--;
        if(this.totalBricks<=0)
            this.win();
    }
    loseLife()
    {
        this.lives--;
        this.textLives.text = "lives: " + this.lives;
        if(this.lives > 0)
        {
            this.gameState = EGAMESTATE.WaitingForShoot;
        }
        else
        {
            this.gameOver();
        }
    }
    gameOver()
    {
        this.gameState = EGAMESTATE.Finished;
        this.textCentral.text = 'YOU LOSE\nTap to restart';

        console.log("[BreakoutGame] GAME OVER");
    }
    win()
    {
        this.ball.stop();
        this.gameState = EGAMESTATE.Finished;
        this.textCentral.text = 'YOU WIN\nTap to restart';

        console.log("[BreakoutGame] YOU WIN");
    }

}



