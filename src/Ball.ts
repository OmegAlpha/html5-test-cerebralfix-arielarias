import {Breakout} from './Breakout';
import {Brick} from './Brick';
import {StateGameplay} from './StateGameplay';
import {EGAMESTATE} from './StateGameplay';

//-- these can be changed by linear velocity and add a random angle on launch
const BALL_INIT_SPEED_Y : number = -300;
const BALL_INIT_SPEED_X : number = -75;

export class Ball extends Phaser.Sprite
{
    game : Breakout.BreakoutGame;
    scene : StateGameplay;

    constructor(game : Breakout.BreakoutGame , scene : StateGameplay)
    {


        super(game,game.world.centerX, 485 ,'atlas','ball.png');

        this.game = game;
        this.scene = scene;

        this.anchor.set(0.5);
        this.checkWorldBounds = true;

        this.game.physics.enable(this,Phaser.Physics.ARCADE);

        this.body.collideWorldBounds = true;
        this.body.bounce.set(1);
        this.body.immovable = true;

        this.events.onOutOfBounds.add(this.onLose,this);
    }

    launch()
    {
        
        console.log("[Ball] Launch");
        this.body.immovable = false;

        this.body.velocity.y = BALL_INIT_SPEED_Y;
        this.body.velocity.x = BALL_INIT_SPEED_X;

    }
    onHitPlayer()
    {
        let PlayerVelocity = this.scene.player.movementVelocity;

        if( Math.abs( PlayerVelocity) > 10)
        {
            
            this.body.velocity.x = PlayerVelocity * 10;

        }

        this.body.velocity.x *= 1.01; //-- increase speed on each player bounce
        this.body.velocity.y *= 1.01; //-- increase speed on each player bounce
        
    }
    update()
    {
        if(this.scene.gameState == EGAMESTATE.Playing)
        {
            this.game.physics.arcade.collide(this,this.scene.bricks,
                this.onHitBrick,
                null,this
            );


            

        }
        else if(this.scene.gameState == EGAMESTATE.WaitingForShoot)
        {
            this.x = this.scene.player.x;
            this.y = this.scene.player.y - 18;
        }

        

    }
    onHitBrick(me,collBrick)
    {
       this.body.velocity.x *= 1.005; //-- increase speed on each player bounce
       this.body.velocity.y *= 1.005; //-- increase speed on each player bounce

       let brick : Brick = <Brick>collBrick;
       brick.onHit();
    }
    stop()
    {
       this.body.velocity.y = 0;
       this.body.velocity.x = 0; 
    }
    onLose()
    {
        this.stop();
        this.scene.loseLife();
    }
}