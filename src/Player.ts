

export class Player extends Phaser.Sprite
{
    game : Phaser.Game;

    KEY_SPACEBAR : Phaser.Key;
    CURSORS      : Phaser.CursorKeys;
    
    previousX    : number;
    movementVelocity : number;

    constructor(game :Phaser.Game)
    {
        var yPosition = game.height - 50;        

        super(game,game.world.centerX, yPosition, 'atlas', 'player.png');

        this.game = game;   
        this.scale.x = 3;

        this.anchor.set(0.5);
        this.game.physics.enable(this,Phaser.Physics.ARCADE);
        this.body.collideWorldBounds = true;
        this.body.bounce.set(1);
        this.body.immovable = true; 

        

        console.log("[Player] Player Created");
    }

    update()
    {
         this.previousX = this.x;  

         let newX : number = this.game.input.x;
         newX = Math.max( this.width/2,newX);
         newX = Math.min(newX,this.game.world.width-this.width/2);

         this.x = newX ;

         this.movementVelocity = this.x - this.previousX;



         //console.log(this.movementVelocity);
    }

}
