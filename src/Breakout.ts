
import {Brick} from './Brick';
import {Player} from './Player';
import {Ball} from './Ball';

import {StateGameplay} from './StateGameplay';

export module Breakout
{
    enum EGAMESTATE
    {
        WaitingForShoot,
        Playing,
        GameOver    
    }
    export class BreakoutGame extends Phaser.Game{

        constructor() {

            //-- todo: use a proper defined aspect ratio and make the game capable to auto size screen
            let width : number = 600;
            var height : number  = 600;

            super(width,height, Phaser.AUTO, 'game');

            this.state.add('StatePlaying',StateGameplay,false);

            this.state.start('StatePlaying');

        }
        
        preload()
        {
             
        }

    }


}


