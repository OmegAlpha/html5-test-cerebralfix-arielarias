# README #

## Instructions ##

I used the same structure I was provided, so:

* npm run build
* npm start
* open localhost:8080

## Info / What I Did / Considerations ##



It was my first time using techs like Phaser and Typescript. 
I was working with Unity and UE4 in the last few years, but I had previous web experience, so I needed to update myself a little.

There are missing feature like testing the game in mobile or autosizing the canvas.

Intead, I focused my test/work hours on doing a right coded game with this technologies.

## Time spent ##

I spent around 10 hours between technologies researching and game programming.

## What I would liked to do with more time ##

* Add the missing features of course.
* Add a proper Level Loader selector from a "levels file".
* Add level progress.
* Add scores and save game.
* Add PowerUps like pills falling from the bricks: bigger paddle, tap to shoot, etc.
* Add Random moving enemies/obstacles.



